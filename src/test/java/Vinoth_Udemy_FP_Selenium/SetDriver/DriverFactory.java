package Vinoth_Udemy_FP_Selenium.SetDriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class DriverFactory {
    WebDriver driver;

    public DriverFactory(WebDriver driver){
        this.driver=driver;
    }
    static Map<String,Supplier<WebDriver>> MAP=new HashMap<>();
    private static final Supplier<WebDriver> chromeDriver=()-> {
        WebDriverManager.chromedriver().setup();
        return new ChromeDriver();
    };
    private static final Supplier<WebDriver> firefoxDriver=()-> {
        WebDriverManager.firefoxdriver().setup();
        return new FirefoxDriver();
    };
    static {
        MAP.put("chrome",chromeDriver);
        MAP.put("firefox",firefoxDriver);

    }
    public WebDriver getDriver(String browser){
       return MAP.get(browser).get();
    }
    public void goTo(WebDriver driver,String url){
      driver.get(url);
    }
}
