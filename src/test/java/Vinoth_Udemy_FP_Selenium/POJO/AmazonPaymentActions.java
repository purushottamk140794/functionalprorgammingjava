package Vinoth_Udemy_FP_Selenium.POJO;
import Vinoth_Udemy_FP.AmazonPaymentScreen;
import org.testng.Assert;

import java.util.function.Consumer;

import static Vinoth_Udemy_FP_Selenium.POJO.PageClass.*;

public class AmazonPaymentActions {
    public static final Consumer<AmazonPaymentScreen> freeCoupon = p->p.addCoupon(FREEPROMOCODE);
    public static final Consumer<AmazonPaymentScreen> discountCoupon = p->p.addCoupon(DISCOUNTPROMOCODE);
    public static final Consumer<AmazonPaymentScreen> validCC = p->p.enterAccountDetail(VALID_CC,VALID_YEAR,VALID_CVV);
    public static final Consumer<AmazonPaymentScreen> buy = AmazonPaymentScreen::clickOnBuyButton;
    public static final Consumer<AmazonPaymentScreen> inValidCC = p->p.enterAccountDetail(INVALID_CC,VALID_YEAR,VALID_CVV);


    public static final Consumer<AmazonPaymentScreen> successFullPayment = p-> Assert.assertEquals(p.verifyStatus(),"PASS");
    public static final Consumer<AmazonPaymentScreen> failedPayment = p-> Assert.assertEquals(p.verifyStatus(),"FAIL");
}
