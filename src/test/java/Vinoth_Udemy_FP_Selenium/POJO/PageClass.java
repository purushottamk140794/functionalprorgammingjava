package Vinoth_Udemy_FP_Selenium.POJO;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;

import java.util.List;
import java.util.function.Predicate;
import static Vinoth_Udemy_FP.AmazonPaymentScreen.*;

import static Vinoth_Udemy_FP_Selenium.POJO.AmazonPaymentActions.*;

public class PageClass {
    final static Predicate<List<WebElement>> allMale = allMale -> allMale.get(1).getText().equalsIgnoreCase("Male");
    final static Predicate<List<WebElement>> allFemale = allMale -> allMale.get(1).getText().equalsIgnoreCase("Female");
    final static Predicate<List<WebElement>> allAu = allMale -> allMale.get(2).getText().equalsIgnoreCase("AU");
    final static Predicate<List<WebElement>> allFemaleHavingAU = allFemale.and(allAu);
    final static Predicate<List<WebElement>> allMaleHavingAU = allMale.and(allAu);

    public final static String FREEPROMOCODE = "FREEUDEMY";
    public final static String DISCOUNTPROMOCODE = "PARTIALUDEMY";
    public final static String VALID_CC = "4111111111111111";
    public final static String INVALID_CC = "4112111111111111";
    public final static String VALID_YEAR = "2023";
    public final static String VALID_CVV = "123";

    @DataProvider(name = "predicate")
    public static Object[] filters() {
        return new Object[]{
                allMale,
                allFemale,
                allFemaleHavingAU,
                allMaleHavingAU
        };
    }

    @DataProvider(name = "textToHover")
    public static Object[][] textToHover() {
        return new Object[][]{
                {"Dropdown => Dropdown Link 2"},
                {"Dropdown => Dropdown Link 5 => Dropdown Submenu Link 5.1"}
        };
    }
    @DataProvider(name = "testDataAmazonScreen")
    public Object[] testDataAmazonScreen() {
        return new Object[]{
                validCC.andThen(buy).andThen(successFullPayment),
                freeCoupon.andThen(buy).andThen(successFullPayment),
                discountCoupon.andThen(validCC).andThen(buy).andThen(successFullPayment),
                inValidCC.andThen(buy).andThen(failedPayment),
                inValidCC.andThen(discountCoupon).andThen(buy).andThen(failedPayment),
                buy.andThen(failedPayment)
        };
    }
}
