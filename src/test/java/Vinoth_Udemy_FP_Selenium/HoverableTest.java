package Vinoth_Udemy_FP_Selenium;

import Vinoth_Udemy_FP_Selenium.POJO.PageClass;
import Vinoth_Udemy_FP_Selenium.SetDriver.*;
import com.google.common.util.concurrent.Uninterruptibles;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.*;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class HoverableTest {
    WebDriver driver;
    DriverFactory driverFactory;
    Actions actions;

    @BeforeTest
    public void setDriver() {
        String url = "https://vins-udemy.s3.amazonaws.com/java/html/drop-down.html#";
        this.driverFactory = new DriverFactory(driver);
        this.driver = driverFactory.getDriver("chrome");
        this.actions = new Actions(driver);
        driverFactory.goTo(driver, url);
    }

    @Test(dataProvider = "textToHover", dataProviderClass = PageClass.class)
    public void selectAndHover(String hoverablText) {
        String[] allTexts = hoverablText.split("=>");
        Arrays.stream(allTexts)
                .map(String::trim)
                .peek(System.out::println)
                .map(By::linkText)
                .map(ele -> this.driver.findElement(ele))
                //.forEach(WebElement::click);
                .forEach(ele -> this.actions.moveToElement(ele).build().perform());

        Uninterruptibles.sleepUninterruptibly(3, TimeUnit.SECONDS);

    }

    @AfterTest
    public void quitDriver() {
        this.driver.quit();
    }
}

