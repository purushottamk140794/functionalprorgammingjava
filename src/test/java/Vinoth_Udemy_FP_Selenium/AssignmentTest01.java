package Vinoth_Udemy_FP_Selenium;

import Vinoth_Udemy_FP_Selenium.SetDriver.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.List;

import static java.sql.DriverManager.getDriver;

public class AssignmentTest01{
    WebDriver driver;
    DriverFactory driverFactory;
    @BeforeTest
    public void setDriver() {
        String url="https://www.google.com";
        this.driverFactory=new DriverFactory(driver);
        this.driver = driverFactory.getDriver("firefox");
        driverFactory.goTo(driver,url);
    }
    @Test
    public void testGoogle(){
       List<WebElement> element=this.driver.findElements(By.tagName("a"));
        element.stream()
                .map(WebElement::getText)
                .map(String::trim)
                .filter(ele->ele.length()>0)
                .filter(ele->!ele.toLowerCase().contains("s"))
                .map(String::toUpperCase)
                .forEach(System.out::println);
    }
    @AfterTest
    public void quitDriver(){
        this.driver.quit();
    }
}

