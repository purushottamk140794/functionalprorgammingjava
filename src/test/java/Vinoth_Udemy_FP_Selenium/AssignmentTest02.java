package Vinoth_Udemy_FP_Selenium;

import Vinoth_Udemy_FP_Selenium.POJO.PageClass;
import Vinoth_Udemy_FP_Selenium.SetDriver.*;
import com.google.common.util.concurrent.Uninterruptibles;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

public class AssignmentTest02 {
    WebDriver driver;
    DriverFactory driverFactory;
    @BeforeTest
    public void setDriver() {
        String url="https://vins-udemy.s3.amazonaws.com/java/html/java8-stream-table.html";
        this.driverFactory=new DriverFactory(driver);
        this.driver = driverFactory.getDriver("firefox");
        driverFactory.goTo(driver,url);
    }

    @Test(dataProvider = "predicate", dataProviderClass = PageClass.class)
    public void selectCheckBoxBasedOnConditionPerDataProvider(Predicate<List<WebElement>> listPredicate) {
        List<WebElement> element = driver.findElements(By.tagName("tr"));
        element.stream().skip(1)
                .map(tr -> tr.findElements(By.tagName("td")))
                .filter(listPredicate)
                .map(tdList -> tdList.get(3).findElement(By.tagName("input")))
                .forEach(WebElement::click);
        Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);
    }

    @AfterTest
    public void quitDriver() {
        this.driver.quit();
    }
}

