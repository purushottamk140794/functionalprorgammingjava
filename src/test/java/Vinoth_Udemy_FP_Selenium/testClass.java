package Vinoth_Udemy_FP_Selenium;

import org.testng.annotations.*;

// Before Test
// before Method
//Test
// after Method
// after Method
//After Test
public class testClass {
    @AfterTest
    public void afterTest(){
        System.out.println("After Test");
    }
    @Test
    public void test(){
        System.out.println("Test");
    }
    @BeforeTest
    public void beforeTest(){
        System.out.println("Before Test");
    }
    @BeforeMethod
    public void beforeMethod(){
        System.out.println("before Method");
    }
    @AfterMethod
    public void afterMethod(){
        System.out.println("after Method");
    }
    public void method(){
        System.out.println("Method");

    }

}
