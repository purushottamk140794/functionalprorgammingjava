package Vinoth_Udemy_FP_Selenium;

import Vinoth_Udemy_FP.AmazonPaymentScreen;

import Vinoth_Udemy_FP_Selenium.POJO.PageClass;
import Vinoth_Udemy_FP_Selenium.SetDriver.DriverFactory;
import com.google.common.util.concurrent.Uninterruptibles;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import static Vinoth_Udemy_FP_Selenium.POJO.PageClass.*;

public class AmazonPaymentTest {
    WebDriver driver;
    DriverFactory driverFactory;
    AmazonPaymentScreen amazonPaymentScreen;
    String url = "https://vins-udemy.s3.amazonaws.com/java/html/java8-payment-screen.html";

    @BeforeTest
    public void setDriver() {
        this.driverFactory = new DriverFactory(driver);
        this.driver = driverFactory.getDriver("chrome");
        this.amazonPaymentScreen = new AmazonPaymentScreen(driver);
        this.driver.manage().window().maximize();
    }

    @Test(description = "Purchase should be successful using valid CC", dataProvider = "testDataAmazonScreen", dataProviderClass = PageClass.class)
    public void selectMinPriceAndVerify(Consumer<AmazonPaymentScreen> consumer) {
        driverFactory.goTo(driver, url);
        consumer.accept(amazonPaymentScreen);
        Uninterruptibles.sleepUninterruptibly(3, TimeUnit.SECONDS);
    }

    @AfterTest
    public void quitDriver() {
        this.driver.quit();
    }
}


   /* Test cases
    Purchase should be successful using valid CC
        Purchase should be successful using discounted promocode + valid CC
        Purchase should be successful using FREE promocode
        Purchase should fail using discounted promocode + invalid CC
        Purchase should fail using invalid CC
        Purchase should fail without any payment info*/