package Vinoth_Udemy_FP_Selenium;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AssignmentTest03 {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get(System.getProperty("user.dir")+"/first-names.txt");
        List<String> allTextList = Files.readAllLines(path);

        //Print count of names which starts with B
       long list1= allTextList.stream().filter(s->s.startsWith("B"))
                .count();
        System.out.println("List 1 ::"+list1);

        //create List of name which starts with C and contain s in it.

        List<String> list2=allTextList.stream().filter(s->s.startsWith("C"))
                            .filter(s->s.contains("s"))
                            .collect(Collectors.toList());
        System.out.println("List 2 ::"+list2.size());

        int sum = allTextList.stream().filter(s->s.startsWith("M"))
                            .map(String::trim)
                            .map(String::length)
                            .mapToInt(i->i)
                            .sum();
        System.out.println("List 3 ::"+sum);

        //Find the names containing - in it and replace it with space and collect into a list

        List<String> list4= allTextList.stream().filter(s->s.contains("-"))
                                                .map(s->s.replace("-"," "))
                                        .collect(Collectors.toList());
        System.out.println("List 4 ::"+list4);

        //Find the names which has more number of chars
        System.out.println(
        allTextList.stream()
                .max(Comparator.comparing(String::length)).get()
        );
    }
}
