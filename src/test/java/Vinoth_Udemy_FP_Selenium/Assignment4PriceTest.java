package Vinoth_Udemy_FP_Selenium;

import Vinoth_Udemy_FP.PriceTable;
import Vinoth_Udemy_FP_Selenium.POJO.PageClass;
import Vinoth_Udemy_FP_Selenium.SetDriver.*;
import com.google.common.util.concurrent.Uninterruptibles;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

public class Assignment4PriceTest {
    WebDriver driver;
    DriverFactory driverFactory;
    PriceTable priceTable;

    @BeforeTest
    public void setDriver() {
        String url = "https://vins-udemy.s3.amazonaws.com/java/html/java8-stream-table-price.html";
        this.driverFactory = new DriverFactory(driver);
        this.driver = driverFactory.getDriver("chrome");
        driverFactory.goTo(driver, url);
        this.priceTable = new PriceTable(driver);
    }

    @Test()
    public void selectMinPriceAndVerify() {
        priceTable.selectMinPrice();
        Assert.assertEquals(priceTable.isStatus(),"PASS");
        Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);

    }

    @AfterTest
    public void quitDriver() {
        this.driver.quit();
    }
}

