package codalityassesment;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MissingInteger {
    public static void main(String[] args) {

        System.out.println(solution(new int[]{1, 3, 6, 4, 1, 2}));
        System.out.println(solution(new int[]{-1, -3, 1}));
        System.out.println(solution(new int[]{1, 2, 3, 4}));
        System.out.println(solution(new int[]{2}));
        System.out.println(solution(new int[]{1, 3}));
    }

    public static int solution(int[] A) {

        List<Integer> li = Arrays.stream(A).parallel().mapToObj(i -> Integer.valueOf(i)).filter(s->s>0).collect(Collectors.toList());
            int minValue = 0;
            for (int i = 1; i <= li.size(); i++) {
                if (!li.contains(i)) {
                    minValue = i;
                    break;
                }
            }
            if (minValue == 0) {
                minValue = li.size() + 1;
            }
            return minValue;
        }
    }
