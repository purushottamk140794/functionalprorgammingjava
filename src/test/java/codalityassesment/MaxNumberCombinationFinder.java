package codalityassesment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MaxNumberCombinationFinder {
    public static void main(String[] args) {
        System.out.println(solution(new int[]{2, 1, 2, 3, 2, 2}, 3));

    }
    public static int solution(int [] A, int R){
        List<Integer> listOfInteger = Arrays.stream(A).skip(R).mapToObj(Integer::valueOf).collect(Collectors.toList());
        int[] intArray = listOfInteger.stream().mapToInt(Integer::intValue).toArray();

        List<List<Integer>> combinations = findCombinations(intArray);

        System.out.println("Combinations: " + combinations.size());
        return combinations.size();
    }

    public static List<List<Integer>> findCombinations(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        Arrays.sort(nums); // Sort the array to handle duplicate combinations
        backtrack(result, new ArrayList<>(), nums, new boolean[nums.length]);
        return result;
    }

    private static void backtrack(List<List<Integer>> result, List<Integer> temp, int[] nums, boolean[] used) {
        if (temp.size() == nums.length) {
            result.add(new ArrayList<>(temp));
            return;
        }

        for (int i = 0; i < nums.length; i++) {
            if (used[i] || (i > 0 && nums[i] == nums[i - 1] && !used[i - 1])) {
                continue;
            }
            temp.add(nums[i]);
            used[i] = true;
            backtrack(result, temp, nums, used);
            temp.remove(temp.size() - 1);
            used[i] = false;
        }
    }

    public static List<List<Integer>> findMaxNumberCombinations(List<List<Integer>> combinations) {
        List<List<Integer>> maxNumberCombinations = new ArrayList<>();
        int maxNumber = Integer.MIN_VALUE;
        for (List<Integer> combination : combinations) {
            int currentNumber = combination.get(0);
            if (currentNumber > maxNumber) {
                maxNumber = currentNumber;
                maxNumberCombinations.clear();
                maxNumberCombinations.add(combination);
            } else if (currentNumber == maxNumber) {
                maxNumberCombinations.add(combination);
            }
        }
        return maxNumberCombinations;
    }
}
