package codalityassesment;

import java.util.Random;

public class Palindrome {

    public static void main(String[] args) {
        //String str = "?ab??a";
        //String str = "?ab???a";
        //String str = "bab??a";
        String str = "?a?b";
        System.out.println(solution(str));
    }
    public static String solution(String s){
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == '?' && !(chars[(chars.length - 1) - i] == '?')) {
                chars[i] = (chars[(chars.length - 1) - i]);
            }
            else if (!(chars[i] == '?') && chars[(chars.length - 1) - i] == '?') {
                chars[(chars.length - 1) - i] = chars[i];
            }
            else if ((chars[i] == '?') && chars[(chars.length - 1) - i] == '?') {
                chars[i] = randomCharacter();
                chars[(chars.length - 1) - i] = chars[i];
                chars[i] = (chars[(chars.length - 1) - i]);
            }
        }
        if(isPalindrome(String.valueOf(chars)))
            return String.valueOf(chars);
        else
            return "NO";
    }
    public static char randomCharacter() {
        Random random = new Random();
        return (char) ('a' + random.nextInt(26));
    }
    public static boolean isPalindrome(String str) {
        int left = 0, right = str.length() - 1;

        while(left < right)
        {
            if(str.charAt(left) != str.charAt(right))
            {
                return false;
            }
            left++;
            right--;
        }
        return true;
    }
}
