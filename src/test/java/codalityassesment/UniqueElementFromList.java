package codalityassesment;

import javax.swing.text.html.parser.Entity;
import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class UniqueElementFromList {

    public static void main(String[] ar) {

        List<Integer> list = List.of(9, 2, 9, 2, 9, 7, 9);
        getUniqueElement(list);

    }

    public static int getUniqueElement(List<Integer> list) {

        if (list.size() == 0 || list.size() % 2 == 0)
            return -1;
        else
            return findUniqueElementFromList(list);
    }

    private static int findUniqueElementFromList(List<Integer> list) {

        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < list.size(); i++) {
            Integer count = map.get(list.get(i));
            if (count == null) {
                map.put(list.get(i), 1);
            } else {
                map.put(list.get(i), ++count);
            }
        }
        Set<Map.Entry<Integer, Integer>> mapSet = map.entrySet();
        List<Integer> value = new ArrayList<>(mapSet.stream()
                .filter(s -> s.getValue() == 1)
                .collect(
                        Collectors.toMap(
                                s -> s.getKey(), s -> s.getValue()
                        )
                ).keySet());
        return value.get(0);
    }
}
