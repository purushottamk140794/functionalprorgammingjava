package codalityassesment;

public class Task2 {

    public static void main(String[] args) {
        //String str = "BBABAA";
        String str = "BAAABAB";

        int removeCounter = 0;
        int checkCounter = 0;
        for (int i = 0; i < str.length() - 1; i++) {
            if (str.charAt(i) == 'B' && checkCounter==0) {
                removeCounter++;
                str = str.replaceFirst("B", "?");
            }
            else if (str.charAt(i) == 'B' && checkCounter>1) {
                removeCounter++;
                str = str.replaceFirst("A", "?");
            }
            else if (str.charAt(i) == 'A') {
                checkCounter++;
            }
        }
        System.out.println(removeCounter);
    }
}
