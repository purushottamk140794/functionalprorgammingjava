package codalityassesment;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.IntStream;

public class RotationOfArray {
    //CyclicRotation
    //Rotate an array to the right by a given number of steps.
    public static int[] solution(int[] array, int K) {
        int j = 0;
        int n = array.length;
        if (array.length == K) {
            return array;
        } else if (K <= 0) {
            return array;
        } else if (array.length <= 0) {
            return array;
        } else if (K >= array.length) {
            return array;
        } else {
            int[] b = new int[n];
            for (int i = (n - K); i < n; i++) {
                b[j++] = array[i];
                if (i == (n - 1))
                    i = -1;
                if (j == n)
                    break;
            }
            return b;
        }
    }

    //OddOccurrencesInArray
//Find value that occurs in odd number of elements.
    public static int test11(int[] a) {
        int val = 0;
        for (int i = 0; i < a.length - 1; i++) {
            int match = 0;
            for (int j = 0; j < a.length - 1; j++) {
                if (a[i] == a[j])
                    match++;
            }
            if (match == 1)
                val = a[i];
        }
        return val;
    }

    //frog jump
    public static int frogJump(int X, int Y, int D) {
        int jump = 0;

        if (X == Y) {
            return 0;
        } else {
            while (X < Y) {
                X = X + D;
                jump++;
            }
            return jump;
        }
    }

    public static int findMissingElementFromList(int[] array) {
        if (array.length <= 0) {
            return 0;
        }else if (array.length == 1) {
            return array[0];
        } else {
            int actualSum = Arrays.stream(array).sum();
            int totalSum = IntStream.rangeClosed(1, array.length + 1).sum();
            return totalSum - actualSum;

        }
    }

    public static void main(String[] args) {
        int[] A = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] B = new int[]{9, 3, 9, 3, 9, 17, 9};

        int K = 2;
        System.out.println("Before shifting:: " + Arrays.toString(A));
        System.out.println(
                Arrays.toString(solution(A, K))

        );
        System.out.println("unpaired integer:: " + test11(B));

        System.out.println("Total frog jump ::" + frogJump(10, 85, 30));


        findMissingElementFromList(new int[]{2, 3, 1, 5});
    }
}
