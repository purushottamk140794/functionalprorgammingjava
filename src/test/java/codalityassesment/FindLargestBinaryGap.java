package codalityassesment;
//https://app.codility.com/programmers/lessons/1-iterations/

// Lesson 1: Iterations
public class FindLargestBinaryGap {
    public static void main(String[] args) {
        int value = 647;

        System.out.println(Integer.toBinaryString(value));
        char[] chars = Integer.toBinaryString(value)
                .toCharArray();
        int overallCount = 0;
        int checkForGap = 0;
        for (char aChar : chars) {
            if (aChar == '0') {
                overallCount = overallCount + 1;
            } else if (aChar == '1') {
                if (overallCount >= checkForGap) {
                    checkForGap = overallCount;
                }
                overallCount = 0;

            }
        }
        System.out.println("Max number of zeroes are :: " + checkForGap);
    }

    private static void calculateTheZeros(StringBuilder append) {

        try {
            System.out.println(append);
        } catch (IndexOutOfBoundsException e) {
            System.out.println(e.getMessage());
        }
    }
}
