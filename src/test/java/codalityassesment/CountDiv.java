package codalityassesment;

public class CountDiv {
    public static void main(String[] args) {
        System.out.println(
                solution(6, 11, 2)
        );
        System.out.println(
                solution1(6, 11, 2)
        );
    }

    private static int solution1(int A, int B, int K) {
        if (A > B)
            return -1;
        if (K == 0)
            return -1;
        else {
            int val = (B / K) - (A / K);
            if (A % K == 0) {
                val++;
            }
            return val;
        }
}

    public static int solution(int A, int B, int K) {
        if (A > B)
            return -1;
        else if (K == 0 || K == 1)
            return -1;
        else {
            int count = 0;
            for (int i = A; i <= B; i++) {
                if (i % K == 0) {
                    count++;
                }
            }
            return count;
        }
    }
}
