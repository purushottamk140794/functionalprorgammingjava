package codalityassesment;

import java.util.HashMap;
import java.util.Map;

public class GenomicRangeQuery {

    public static void main(String[] args) {
        int[] P = new int[3];
        int[] Q = new int[3];

        P[0] = 2;
        Q[0] = 4;
        P[1] = 5;
        Q[1] = 5;
        P[2] = 0;
        Q[2] = 6;

        solution("CAGCCTA", P, Q);
    }

    public static int[] solution(String S, int[] P, int[] Q) {
        if (P.length != Q.length) {
            return null;
        } else if (S.length() <= 0)
            return null;
        else if (!S.matches("^[ACGT]+$"))
            return null;
        else {
            int[] minimal = new int[P.length];
            for (int i = 0; i < P.length; i++) {
                String charsFromParticularIndex = S.substring(P[i], Q[i] + 1);
                minimal[i] = getMinValue(getValueSetForDNA(charsFromParticularIndex));
            }
            return minimal;
        }
    }

    private static Map<Character, Integer> getValueSetForDNA(String charsFromParticularIndex) {
        Map<Character, Integer> map = new HashMap<>();

        if (charsFromParticularIndex.chars().parallel().mapToObj(i -> (char) i).allMatch(c -> c == charsFromParticularIndex.charAt(0))) {
            Character eachChar = charsFromParticularIndex.charAt(0);
            if (eachChar.equals('A'))
                map.put('A', 1);
            else if (eachChar.equals('C'))
                map.put('C', 2);
            else if (eachChar.equals('G'))
                map.put('G', 3);
            else if (eachChar.equals('T'))
                map.put('T', 4);
        }else {
            charsFromParticularIndex.chars().parallel().mapToObj(i -> (char) i)
                    .forEach(eachChar ->
                    {
                        if (eachChar.equals('A'))
                            map.put('A', 1);
                        else if (eachChar.equals('C'))
                            map.put('C', 2);
                        else if (eachChar.equals('G'))
                            map.put('G', 3);
                        else if (eachChar.equals('T'))
                            map.put('T', 4);
                    });
        }
        return map;
    }

    public static int getMinValue(Map<Character, Integer> map) {
        int minValue = Integer.MAX_VALUE;
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            int value = entry.getValue();
            if (value < minValue) {
                minValue = value;
            }
        }
        return minValue;
    }
}