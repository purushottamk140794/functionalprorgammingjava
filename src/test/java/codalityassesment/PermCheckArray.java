package codalityassesment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PermCheckArray {
    public static void main(String[] args) {

        System.out.println(

                solution(new int[]{1,2})


        );
    }

    public static int solution(int[] A) {

        int N = A.length;
        if(A.length>1){
        Set<Integer> set = new HashSet<>();
        for (Integer i : A) {
            if(!set.add(i)){
                return 0;
            }
        }
        for (int i = 1; i <= N; i++) {

            if (!set.contains(i)) {
                return 0;
            }
        }
        return 1;

        }
        else if(A.length==1){
            return 0;
        }
        return 0;
    }
}
