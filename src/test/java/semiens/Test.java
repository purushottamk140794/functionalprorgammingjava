package semiens;

import java.util.*;
import java.util.stream.Collectors;

//Ask for user input
//find out highest occuring character
//print it in descending order
// I am purushottam and I am giving interview
public class Test {

    public static void main(String[] args) {

        String str = "I am Purushottam and I am giving interview";

        List<Character> characterList = str.chars().mapToObj(c->(char)c)
                .collect(Collectors.toList());
        Map<Character,Integer> map = new HashMap<>();
         for(Character c : characterList){
            Integer count = map.get(c);
            if(count==null){
                map.put(c,1);
            } else
                map.put(c,++count);
         }
        Map<Character,Integer> map1 = map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        System.out.println(map1);

    }
}
