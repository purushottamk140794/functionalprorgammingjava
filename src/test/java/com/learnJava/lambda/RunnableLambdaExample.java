package com.learnJava.lambda;

public class RunnableLambdaExample {
    public static void main(String[] args) {

        /**
         * Prior Java 8
         */
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("Runner Interface 1");
            }
        };
        new Thread(runnable).start();

        /**
         * Java 8
         */
        Runnable runnable1 = () -> System.out.println("Runner Interface 2");
        new Thread(runnable1).start();

    }
}
