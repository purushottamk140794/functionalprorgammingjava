package com.learnJava.lambda;

import java.util.Comparator;

public class ComparatorLambdaExample {
    public static void main(String[] args) {

        /**
         * Comparator prior version 8
         */

        Comparator<Integer> comparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2); // o1>o2 => 1
                // o1<o2 => -1
                // o1=o2 =>  0
            }
        };
        System.out.println(comparator.compare(2, 3));

        /**
         * using lambda
         */
        Comparator<Integer> comparator1 = (Integer o1, Integer o2) -> o1.compareTo(o2);
        System.out.println(comparator1.compare(3, 2));
    }

}
