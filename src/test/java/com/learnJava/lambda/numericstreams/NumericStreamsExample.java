package com.learnJava.lambda.numericstreams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class NumericStreamsExample {
    static List<Integer> list = Arrays.asList(1,3,5,6,3,1);

    public static int sumOfNNumbers(List<Integer> ls){
        int sum = ls.stream().reduce((a,b)->a+b).get();
        return sum;
    }
    public static int sumOfNNumberUsingIntStream(){
        return list.stream().//Wrapper to primitive :: Unboxing
                mapToInt(s->s).sum();
    }
    public static void boxing(){
        //primitive type to Wrapper type: int to Integer
        System.out.println(
        IntStream.range(1,20).boxed().collect(Collectors.toList()));
    }
    public static void main(String[] args) {
        boxing();
        System.out.println(sumOfNNumberUsingIntStream());
    }




}
