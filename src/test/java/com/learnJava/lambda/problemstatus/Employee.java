package com.learnJava.lambda.problemstatus;

import java.util.List;
import java.util.function.Function;

public class Employee{

    private String name;
    private String designation;
    private List<String> technicalSkills;

    public Employee(String name, String designation, List<String> technicalSkills) {
        this.name = name;
        this.designation = designation;
        this.technicalSkills = technicalSkills;
    }

    public List<String> getTechnicalSkills() {
        return technicalSkills;
    }

    public void setTechnicalSkills(List<String> technicalSkills) {
        this.technicalSkills = technicalSkills;
    }


    public Employee(String name, String designation) {
        this.name = name;
        this.designation = designation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    @Override
    public String toString() {
        return "Employee{" + designation + ":" + technicalSkills + ":" + getName() + "}";
    }
}
