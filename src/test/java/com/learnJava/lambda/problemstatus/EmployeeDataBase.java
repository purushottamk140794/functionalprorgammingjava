package com.learnJava.lambda.problemstatus;

import java.util.*;
import java.util.stream.Collectors;

public class EmployeeDataBase {
    public static List<Employee> getAllEmployeeWithTechnicalSkills() {
        Employee employee1_1 = new Employee("Rakesh", "QA", Arrays.asList("Manual Testing", "Functional Testing", "Smoke Testing"));
        Employee employee1_2 = new Employee("Ram", "QA", Arrays.asList("Time pass Testing", "Funky Testing", "Smoke Testing"));
        Employee employee1_3 = new Employee("Manoj", "HR", Arrays.asList("Rongoli Banana", "Lunch Break"));
        Employee employee1_4 = new Employee("Pankan", "QA", Arrays.asList("Manual Testing", "Functional Testing", "Smoke Testing"));
        Employee employee1_5 = new Employee("Prakash", "HR", Arrays.asList("Rongoli Banana", "Lunch Break"));
        Employee employee1_6 = new Employee("Atul", "HR", Arrays.asList("Rongoli Banana", "Lunch Break"));
        Employee employee1_7 = new Employee("Jalpan", "HR", Arrays.asList("Rongoli Banana", "Lunch Break"));
        Employee employee1_8 = new Employee("Mohan", "QA", Arrays.asList("Manual Testing", "Functional Testing", "Smoke Testing"));
        Employee employee1_9 = new Employee("Saroj", "HR", Arrays.asList("Rongoli Banana", "Lunch Break"));
        Employee employee1_10 = new Employee("Preetesh", "HR", Arrays.asList("Rongoli Banana", "Lunch Break"));
        Employee employee1_11 = new Employee("Purushottam", "QA", Arrays.asList("Manual Testing", "Functional Testing", "Smoke Testing"));

        List<Employee> listOfEmployee1 = Arrays.asList(employee1_1, employee1_2, employee1_3, employee1_4, employee1_5,
                employee1_6, employee1_7, employee1_8, employee1_9, employee1_10, employee1_11);
        return listOfEmployee1;
    }

    public static List<Employee> getAllEmployee() {
        Employee employee1 = new Employee("Rakesh", "QA");
        Employee employee2 = new Employee("Purushottam", "QA");
        Employee employee3 = new Employee("Manoj", "HR");
        Employee employee4 = new Employee("Pankan", "QA");
        Employee employee5 = new Employee("Prakash", "HR");
        Employee employee6 = new Employee("Atul", "HR");
        Employee employee7 = new Employee("Jalpan", "HR");
        Employee employee8 = new Employee("Mohan", "QA");
        Employee employee9 = new Employee("Saroj", "HR");
        Employee employee10 = new Employee("Preetesh", "HR");
        Employee employee11 = new Employee("Purushottam", "QA");

        List<Employee> listOfEmployee = Arrays.asList(employee1, employee2, employee3, employee4, employee5,
                employee6, employee7, employee8, employee9, employee10, employee11);
        return listOfEmployee;
    }

    public void getDataGroupByDesignation() {

        //group employee with its designation

        //Before Java 8
        Map<String, List<String>> map = new HashMap<>();
        getAllEmployee().forEach(
                s -> {
                    List<String> list = new ArrayList<>();
                    if (!(map.containsKey(s.getDesignation()))) {
                        list.add(s.getName());
                        map.put(s.getDesignation(), list);
                    } else {
                        map.get(s.getDesignation()).add(s.getName());
                    }
                }
        );
        System.out.println(
                map.entrySet().stream().collect(Collectors.toMap(
                        Map.Entry::getKey, a -> new ArrayList<>(a.getValue())
                ))
        );
        System.out.println(map.get("QA"));
        System.out.println("USING Grouping" + getAllEmployee().stream().collect(Collectors.groupingBy(Employee::getDesignation, Collectors.collectingAndThen(Collectors.toList(), list -> list.stream().map(Employee::getName)
                .collect(Collectors.toList())))));
        System.out.println("USING Grouping 1 " + getAllEmployee().stream().collect(
                Collectors.groupingBy(Employee::getDesignation, Collectors.mapping(Employee::getName, Collectors.toList()))));

    }

    public static void main(String[] args) {

        //group employee with its designation

        //after Java 8
        System.out.println("USING Grouping 1 " +
                getAllEmployee().stream().collect(Collectors.groupingBy(Employee::getDesignation))
        );
        //Map<String, Map<String, List<String>>> groupedEmployees = new HashMap<>();

        System.out.println("USING Grouping 2 " +
                getAllEmployeeWithTechnicalSkills().stream().collect(Collectors.groupingBy(Employee::getDesignation, Collectors.groupingBy(
                        s->s.getTechnicalSkills().stream().findFirst().orElse("Error"),Collectors.mapping(
                                Employee::getName,Collectors.toList())))));


/*        getAllEmployeeWithTechnicalSkills().stream()
                .flatMap(emp -> emp.getTechnicalSkills().stream()
                .map(techSkills -> new AbstractMap.SimpleEntry(emp.getDesignation(),
                        new AbstractMap.SimpleEntry(techSkills, emp.getName())))
                .collect(Collectors.groupingBy(Map.Entry::getKey, Collectors.groupingBy())));*/

    }
}
