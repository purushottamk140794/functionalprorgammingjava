package com.learnJava.lambda.optional;

import data.Bike;
import data.Student;
import data.StudentDatabase;

import java.util.Optional;

public class optionalMapFilterFlatMap {

    public static void optionalFilter(){
        Optional<Student> optionalStudent = Optional.ofNullable(StudentDatabase.optionalSupplier.get());
        Optional<Student> optionalStudent1 = Optional.empty();

        String name = optionalStudent.filter(s -> s.getGender().equals("male")).isPresent() ? optionalStudent.get().getName() : "kd";
        System.out.println(name);
    }
    public static void optionalMap(){
        Optional<Student> optionalStudent = Optional.ofNullable(StudentDatabase.optionalSupplier.get());
        Optional<Student> optionalStudent1 = Optional.empty();

        String name = optionalStudent.map(s->s.getGender()).isPresent() ? optionalStudent.get().getName() : "kd";
        System.out.println(name);
    }
    public static void optionalFlatMap(){
        Optional<Student> optionalStudent = Optional.ofNullable(StudentDatabase.optionalSupplier.get());
        Optional<Student> optionalStudent1 = Optional.empty();

        Optional<Bike> name = optionalStudent
                .filter(s -> s.getGender().equals("male")) //Optional<Student>
                .flatMap(Student::getBike); //Optional<Bike>
        System.out.println(name.get().getName());
    }
    public static void main(String[] args) {

        //optionalFilter();
        //optionalMap();
        optionalFlatMap();
    }
}
