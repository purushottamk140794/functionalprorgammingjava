package com.learnJava.lambda.optional;

import data.Student;
import data.StudentDatabase;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Supplier;

public class OrElseGetThrow {
    public static Supplier<Student> optionalSupplier = () -> new Student("updatedName",1,4.1, "female",10, Arrays.asList("swimming", "basketball","volleyball"));

    public static void orElse(){
       Optional<Student> optionalStudent = Optional.ofNullable(StudentDatabase.optionalSupplier.get());
        Optional<Student> optionalStudent1 = Optional.empty();

       String name = optionalStudent1.map(Student::getName).orElse("Set Default Name");
        System.out.println(name);
    }
    public static void orElseGet(){
        Optional<Student> optionalStudent = Optional.ofNullable(StudentDatabase.optionalSupplier.get());
        Optional<Student> optionalStudent1 = Optional.empty();

        String name = optionalStudent1.map(Student::getName).orElseGet(() -> "updatedName");
        System.out.println(name);
    }
    public static void orElseThrow(){
        Optional<Student> optionalStudent = Optional.ofNullable(StudentDatabase.optionalSupplier.get());
        Optional<Student> optionalStudent1 = Optional.empty();

        String name = optionalStudent1.map(Student::getName).orElseThrow(() -> new RuntimeException("Data not Found"));
        System.out.println(name);
    }

    public static void main(String[] args) {
        orElse();
        orElseGet();
        orElseThrow();
    }
}
