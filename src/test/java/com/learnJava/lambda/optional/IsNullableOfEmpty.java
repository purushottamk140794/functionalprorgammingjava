package com.learnJava.lambda.optional;

import java.util.Optional;

public class IsNullableOfEmpty {

    public static void isNullable(){
        Optional<String> stringOptional = Optional.ofNullable(null);
        System.out.println(stringOptional);
    }
    public static void of(){
        Optional<String> stringOptional = Optional.of(null);
        System.out.println(stringOptional);
    }
    public static void empty(){
        Optional<String> stringOptional = Optional.empty();
        System.out.println(stringOptional);
    }
    public static void main(String[] args) {
        isNullable();
        //of();
        empty();
    }
}
