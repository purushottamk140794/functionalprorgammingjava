package com.learnJava.lambda.streams;

import data.Student;
import data.StudentDatabase;

import java.util.function.BinaryOperator;

public class StudentReduceStream {


    static BinaryOperator<Student> binaryOperator = (s1, s2) -> (s1.getGpa() > s2.getGpa()) ? s1 : s2;


    public static void main(String[] args) {
        double student = StudentDatabase.getAllStudents().
                stream().reduce(binaryOperator).get().getGpa();

        System.out.println(

                student
        );
    }
}
