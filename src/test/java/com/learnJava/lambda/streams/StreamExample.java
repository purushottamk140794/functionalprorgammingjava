package com.learnJava.lambda.streams;

import data.Student;
import data.StudentDatabase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class StreamExample {

    public static void main(String[] args) {
        Map<String, List<String>>
                listMap = new HashMap<>();
        StudentDatabase.getAllStudents().parallelStream()
                .forEach(
                        student -> {

                            listMap.put(student.getName(), student.getActivities());
                        }

                );
        Map<String, List<String>> l = listMap.entrySet().stream().collect(Collectors.toMap(
                Map.Entry::getKey,
                Map.Entry::getValue
        ));

        Function<Student, String> nameFunction = Student::getName;
        Function<Student, List<String>> listOfActivitiesFunction = Student::getActivities;

        System.out.println(
                StudentDatabase.getAllStudents().stream().
                        filter(s -> s.getGpa() > 3.9).
                        collect(Collectors.toMap(
                                nameFunction, listOfActivitiesFunction
                        )));

    }
}
