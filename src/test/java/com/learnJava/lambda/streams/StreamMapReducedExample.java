package com.learnJava.lambda.streams;

import data.Student;
import data.StudentDatabase;

import java.util.Objects;
import java.util.Optional;

public class StreamMapReducedExample {

    public static Optional<Integer> getNoOfNoteBooks() {
        return StudentDatabase.getAllStudents().stream()
                .filter(student -> student.getGradeLevel() >= 3 && Objects.equals(student.getGender(), "female"))
                .map(Student::getNoteBooks).reduce(Integer::sum);
    }

    public static void main(String[] args) {

        if (getNoOfNoteBooks().isPresent()) {
            System.out.println(getNoOfNoteBooks().get());
        }

    }
}
