package com.learnJava.lambda.streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class StreamMinMaxExample {

    public static Optional<Integer> findMaxValue(List<Integer> integerList) {
        return integerList.stream().reduce((x, y) -> x > y ? x : y);
    }

    public static int findMaxValueOptional(List<Integer> integerList) {
        return integerList.stream().reduce(
                0, (x, y) -> x > y ? x : y
        );
    }

    public static void main(String[] args) {
        List<Integer> li = Arrays.asList(1, 4, 5, 6, 2, 9, 3);
        System.out.println(
                findMaxValue(li)
        );
        List<Integer> li1 = new ArrayList<>();
        if (findMaxValue(li1).isPresent()) {
            System.out.println(findMaxValue(li1).get());
        } else
            System.out.println("List is Empty");
        ;

    }
}