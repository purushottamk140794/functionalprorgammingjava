package com.learnJava.lambda.streams;

import com.learnJava.lambda.problemstatus.Employee;
import com.learnJava.lambda.problemstatus.EmployeeDataBase;
import data.Student;
import data.StudentDatabase;

import java.util.Collection;
import java.util.stream.Collectors;

public class TerminalOperation_toJoining {

    public static void main(String[] args) {
        System.out.println(
                EmployeeDataBase.getAllEmployeeWithTechnicalSkills().stream()
                        .map(s -> {
                            return s.getTechnicalSkills().stream()
                                    .collect(Collectors.joining("::"));
                        })
                        .collect(Collectors.joining("--"))
        );
        System.out.println(
                (Long) EmployeeDataBase.getAllEmployeeWithTechnicalSkills().stream()
                        .map(s -> {
                            return (Long) (long) s.getTechnicalSkills().size();
                        }).count()
        );

        System.out.println(
                 EmployeeDataBase.getAllEmployeeWithTechnicalSkills()
                         .stream()
                         .map(Employee::getTechnicalSkills)
                         .flatMap(Collection::stream)
                         .distinct()
                         .collect(Collectors.toList())

        );
    }
}
