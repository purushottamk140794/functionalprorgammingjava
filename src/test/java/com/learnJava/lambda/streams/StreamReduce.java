package com.learnJava.lambda.streams;

import java.util.Arrays;
import java.util.function.BinaryOperator;

public class StreamReduce {

    public static void main(String[] args) {
        BinaryOperator<Integer> multiplyOperator = (a,b)->(a*b);
        java.util.List<Integer> arrayList = Arrays.asList(1,3,5,7);
        int output = arrayList.stream().reduce(multiplyOperator).get();

        System.out.println(output);

}
}
