package com.learnJava.lambda.streams;

import org.apache.commons.lang3.RandomUtils;

import java.util.Iterator;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class StreamOfGenerateIterateExample {

    public static void main(String[] args) {
        Stream<String> stringStream = Stream.of("Adm", "dan", "Julie");
        // stringStream.forEach(System.out::println);

        Iterator<String> iterator = stringStream.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        Stream.iterate(1, i -> i * 2).limit(4).forEach(System.out::println);
        Supplier<Integer> supplierStream = RandomUtils::nextInt;

        Stream.generate(supplierStream).limit(5).forEach(System.out::println);
    }
}
