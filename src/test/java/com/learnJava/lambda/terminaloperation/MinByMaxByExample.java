package com.learnJava.lambda.terminaloperation;

import data.Student;
import data.StudentDatabase;

import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;

public class MinByMaxByExample {
    public static Optional<Student> minByExample(){
       return StudentDatabase.getAllStudents()
                .stream()
                .collect(Collectors.minBy(
                        Comparator.comparing(s->s.getGpa()
                )));
    }
    public static Optional<Student> maxByExample(){
        return StudentDatabase.getAllStudents()
                .stream()
                .collect(Collectors.maxBy(
                        Comparator.comparing(s->s.getGpa()
                        )));
    }
    public static void main(String[] args) {
        System.out.println(minByExample());
        System.out.println(maxByExample());

    }
}
