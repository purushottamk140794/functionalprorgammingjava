package com.learnJava.lambda.terminaloperation;

import data.StudentDatabase;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CountingExample {
    static List<String> list = Arrays.asList("John","Alan","Murphy","Paul");

    public static void main(String[] args) {
        System.out.println(count());
    }
    public static long count(){
        return StudentDatabase.getAllStudents().stream().filter(s->s.getGpa()>=3.9).collect(Collectors.counting());
    }
}
