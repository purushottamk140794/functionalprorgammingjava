package com.learnJava.lambda.terminaloperation;

import com.learnJava.lambda.problemstatus.Employee;
import com.learnJava.lambda.problemstatus.EmployeeDataBase;
import data.Student;
import data.StudentDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GroupingBy1Example {

    public static Map<Object, List<Student>> groupByGender() {
        return StudentDatabase.getAllStudents()
                .stream()
                .collect(
                        Collectors.groupingBy(
                                s -> s.getGender()
                        )
                );
    }

    public static Map<Object, List<Student>> customizeGroupBy() {

        return StudentDatabase.getAllStudents()
                .stream()
                .collect(
                        Collectors.groupingBy(
                                s -> s.getGpa() > 3.8 ? "OUTSTANDING" : "AVERAGE"
                        )
                );
    }

    public static void twoParameterGroupingBy_1() {

        Map<List<String>, Map<String, List<Student>>> stringMapMap = StudentDatabase.getAllStudents()
                .stream()
                .collect(
                        Collectors.groupingBy(
                                s -> s.getActivities(),
                                Collectors.groupingBy(
                                        s -> s.getName(), Collectors.toList()
                                )
                        )
                );
        System.out.println(stringMapMap);
    }

    public static void twoParameterGroupingBy_2() {

        Map<String, List<String>> stringMapMap = EmployeeDataBase.getAllEmployeeWithTechnicalSkills()
                .stream()
                .collect(
                        Collectors.toMap(
                                s -> s.getName(), s -> s.getTechnicalSkills()
                        )
                );
        Map<String, List<String>> finalMap = new HashMap<>();

        stringMapMap.forEach(
                (key, value) ->
                        value.forEach(
                                eachValue -> {
                                    if (finalMap.get(eachValue) != null && !finalMap.get(eachValue).isEmpty()) {
                                        finalMap.get(eachValue).add(key);
                                    } else {
                                        List<String> list = new ArrayList<>();
                                        list.add(key);
                                        finalMap.put(eachValue, list);
                                    }
                                }
                        )


        );
        System.out.println(finalMap);
    }

    public static void twoParameterGroupingBy_3() {
        List<Employee> stringMapMap = new ArrayList<>(EmployeeDataBase.getAllEmployeeWithTechnicalSkills());
        Map<String, Map<String, List<String>>> finalMap = new HashMap<>();
        Map<String, List<String>> map = new HashMap<>();
        stringMapMap.forEach(
                eachEmployee -> eachEmployee.getTechnicalSkills().forEach(
                        eachTechSkills -> {
                            if (finalMap.get(eachTechSkills) != null && !finalMap.get(eachTechSkills).isEmpty()) {
                                finalMap.get(eachTechSkills).get(eachEmployee.getDesignation()).add(eachEmployee.getName());
                            } else {
                                List<String> list = new ArrayList<>();
                                Map<String, List<String>> map1 = new HashMap<>();
                                list.add(eachEmployee.getName());
                                map1.put(eachEmployee.getDesignation(), list);
                                finalMap.put(eachTechSkills, map1);
                            }
                        }
                )
        );
        System.out.println(finalMap);
    }

    public static void main(String[] args) {

        //System.out.println(customizeGroupBy());
        twoParameterGroupingBy_1();
        twoParameterGroupingBy_2();
        twoParameterGroupingBy_3();
    }
}
