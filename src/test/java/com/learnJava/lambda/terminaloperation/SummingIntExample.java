package com.learnJava.lambda.terminaloperation;

import data.StudentDatabase;

import java.util.stream.Collectors;

public class SummingIntExample {
    public static void main(String[] args) {
        System.out.println("Summing Int " + StudentDatabase.getAllStudents()
                .stream()
                .collect(Collectors.summingInt(
                                s -> s.getNoteBooks()
                        )
                ));

        System.out.println(
                "Averaging int " +
                        StudentDatabase.getAllStudents()
                                .stream()
                                .collect(Collectors.averagingInt(
                                                s -> s.getNoteBooks()
                                        )
                                ));
    }
}
