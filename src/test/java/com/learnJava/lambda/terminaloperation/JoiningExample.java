package com.learnJava.lambda.terminaloperation;

import data.StudentDatabase;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class JoiningExample {
    static List<String> list = Arrays.asList("John","Alan","Murphy","Paul");

    public static void main(String[] args) {
        System.out.println(
        list.stream().collect(Collectors.joining("::")));

        System.out.println(joining_1());
        System.out.println(joining_2());
    }

    public static String joining_1(){
       return StudentDatabase.getAllStudents().stream()
                .map(s->s.getName())
                .collect(Collectors.joining(","));
    }
    public static String joining_2(){
        return StudentDatabase.getAllStudents().stream()
                .map(s->s.getName())
                .collect(Collectors.joining("-","(",")"));
    }

}
