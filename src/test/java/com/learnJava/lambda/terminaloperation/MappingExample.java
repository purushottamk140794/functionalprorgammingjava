package com.learnJava.lambda.terminaloperation;

import data.StudentDatabase;

import java.util.stream.Collector;
import java.util.stream.Collectors;

public class MappingExample {
    public static void main(String[] args) {
        System.out.println(
        StudentDatabase.getAllStudents()
                .stream()
                .collect(Collectors.mapping(
                    s->s.getName(), Collectors.toList()
                )));
    }
}
