package com.learnJava.lambda.functionalinterface;

import data.Student;
import data.StudentDatabase;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Predicate;

public class BiFunctionExample {

    static BiFunction<List<Student>, Predicate<Student>, Map<String, Double>> biFunction = ((students, studentPredicate) -> {
        Map<String, Double> stringDoubleMap = new HashMap<>();
        students.forEach(
                student -> {
                    if (studentPredicate.test(student)) {
                        stringDoubleMap.put(student.getName(), student.getGpa());
                    }
                });
        return stringDoubleMap;
    });

    public static void main(String[] args) {
        System.out.println(
                biFunction.apply(StudentDatabase.getAllStudents(), PredicateExample.p1)
        );
    }
}
