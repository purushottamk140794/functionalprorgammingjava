package com.learnJava.lambda.functionalinterface;

import data.Student;
import data.StudentDatabase;

import java.util.function.Predicate;

public class PredicateExample {

    static java.util.List<Student> allStudents = StudentDatabase.getAllStudents();
    static Predicate<Student> p1 = student -> student.getGpa() > 3.9;
    static Predicate<Student> p2 = student -> student.getGradeLevel() >= 3;

    public static void main(String[] args) {

       // filterStudentGPA();
       // filterStudentAnd();
        filterStudentOr();
    }

    private static void filterStudentOr() {
        allStudents.forEach(
                student -> {
                    if (p1.and(p2).test(student)) {
                        System.out.println("predicate or result:: " + student);
                    }
                }
        );
    }

    private static void filterStudentAnd() {
        allStudents.forEach(
                student -> {
                    if (p1.and(p2).test(student)) {
                        System.out.println("predicate and result:: " + student);
                    }
                }
        );
    }

    private static void filterStudentGPA() {
        allStudents.forEach(
                student -> {
                    if (p1.test(student)) {
                        System.out.println("predicate result :: " + student);
                    }
                }
        );
    }
}
