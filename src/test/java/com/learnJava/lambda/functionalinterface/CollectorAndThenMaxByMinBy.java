package com.learnJava.lambda.functionalinterface;

import data.Student;
import data.StudentDatabase;

import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class CollectorAndThenMaxByMinBy {

    public static void calculateMaxGpa()
    {
        Map<Integer, Optional<Student>> studentMaxByGpa =  StudentDatabase.getAllStudents()
                .stream()
                .collect(
                        Collectors.groupingBy(
                                s->s.getGradeLevel(),
                                Collectors.maxBy(
                                        Comparator.comparing(
                                                s->s.getGpa()
                                        )
                                )
                        )
                );
        System.out.println(studentMaxByGpa);
    } public static void calculateMaxGpaUsingCollectingAndThen()
    {
        Map<Integer, Student> studentMaxByGpa =  StudentDatabase.getAllStudents()
                .stream()
                .collect(
                        Collectors.groupingBy(
                                s->s.getGradeLevel(),
                                Collectors.collectingAndThen(Collectors.maxBy(Comparator.comparing(
                                                s->s.getGpa()
                                        )),Optional::get
                                )
                        )
                );
        System.out.println(studentMaxByGpa);
    }
    public static void main(String[] args) {
        calculateMaxGpa();
        calculateMaxGpaUsingCollectingAndThen();
    }
}
