package com.learnJava.lambda.functionalinterface;

import java.util.Comparator;
import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;

public class UnaryOperatorExample {

    static UnaryOperator<String> unaryOperator = s -> s.concat("default");

    static Comparator<Integer> comparator = Integer::compareTo;

    public static void main(String[] args) {
        System.out.println(
                unaryOperator.apply("java")
        );

        BinaryOperator<Integer> max = BinaryOperator.maxBy(comparator);
        System.out.println("Result Max value is:: "+max.apply(3,4));

        BinaryOperator<Integer> min = BinaryOperator.minBy(comparator);
        System.out.println("Result Min value is:: "+min.apply(3,4));

    }
}
