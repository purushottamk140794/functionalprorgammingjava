package com.learnJava.lambda.functionalinterface;

import data.Student;
import data.StudentDatabase;

import java.util.*;
import java.util.function.Function;

public class FunctionExample {

    static Function<List<Student>, Map<String, Double>> listMapFunction = (students -> {

        Map<String, Double> studentGenderMap = new HashMap<>();

        students.forEach(
                student -> {
                    if (PredicateExample.p2.test(student)) {
                        studentGenderMap.put(student.getName(), student.getGpa());
                    }
                }
        );
        return studentGenderMap;
    });


    public static void main(String[] args) {
        System.out.println(
                listMapFunction.apply(StudentDatabase.getAllStudents())
        );
    }
}
