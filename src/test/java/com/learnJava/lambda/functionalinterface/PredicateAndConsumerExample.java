package com.learnJava.lambda.functionalinterface;

import data.Student;
import data.StudentDatabase;

import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class PredicateAndConsumerExample {
    static Predicate<Student> p1 = student -> student.getGradeLevel() >= 3;
    static Predicate<Student> p2 = student -> student.getGpa() > 3;
    static BiConsumer<String, java.util.List<String>> bc1 = (name, activities) -> System.out.println(name + " : " + activities);
    static java.util.List<Student> allStudents = StudentDatabase.getAllStudents();
    static BiPredicate<Integer, Double> bp1 = (gradeLevel, gpa) -> gradeLevel >= 3 && gpa >= 4;

    public static void main(String[] args) {
        //printPredicateAndBiConsumer();
        printNameActivitiesUsingBiPredicate();
    }

    private static void printNameActivitiesUsingBiPredicate() {
        allStudents.forEach(
                student ->
                {
                    if (bp1.test(student.getGradeLevel(), student.getGpa())) {
                        bc1.accept(student.getName(), student.getActivities());
                    }
                }
        );
    }

    private static void printPredicateAndBiConsumer() {
        allStudents.forEach(student ->
        {
            if (p1.and(p2).test(student)) {
                bc1.accept(student.getName(), student.getActivities());
            }
        });
    }

}
