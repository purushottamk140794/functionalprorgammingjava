package com.learnJava.lambda.functionalinterface;

import data.Student;
import data.StudentDatabase;

import java.util.Locale;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class BiConsumerExample {
    static BiConsumer<String, java.util.List<String>> biConsumerNameActivities = (name, activities) -> System.out.println(name + "::" + activities);
    static Consumer<Student> consumerActivities = s -> System.out.println(s.getActivities());

    public static void main(String[] args) {

        BiConsumer<String, Integer> consumer = (s, i) -> System.out.println(s.toUpperCase(Locale.ROOT) + "::" + i);
        consumer.accept("java", 8);
        printStudentsName();

    }

    public static void printStudentsName() {
        java.util.List<Student> allStudents = StudentDatabase.getAllStudents();
        allStudents.forEach(student ->
        {
            if (student.getGradeLevel() > 3) {
                biConsumerNameActivities.accept(student.getName(), student.getActivities());
            }
        });
    }
}
