package com.learnJava.lambda.functionalinterface;

import data.Student;

import java.util.function.Supplier;

public class ConstructorReferenceExample {

    static Supplier<Student> supplier = Student::new;

    public static void main(String[] args) {
        System.out.println(
        supplier.get());
    }
}
