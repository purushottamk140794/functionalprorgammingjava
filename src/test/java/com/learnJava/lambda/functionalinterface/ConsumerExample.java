package com.learnJava.lambda.functionalinterface;

import data.Student;
import data.StudentDatabase;

import java.util.Locale;
import java.util.function.Consumer;

public class ConsumerExample {
    static Consumer<Student> consumerName = s -> System.out.print(s.getName());
    static Consumer<Student> consumerActivities = s -> System.out.println(s.getActivities());

    public static void main(String[] args) {

        Consumer<String> consumer = s -> System.out.println(s.toUpperCase(Locale.ROOT));
        //consumer.accept("java8");
        //printStudentsName();
        //printStudentNameAndActivities();
        printStudentNameAndActivitiesUsingCondition();
    }

    public static void printStudentsName() {
        java.util.List<Student> allStudents = StudentDatabase.getAllStudents();
        allStudents.forEach(consumerName);
    }

    public static void printStudentNameAndActivities() {
        java.util.List<Student> allStudents = StudentDatabase.getAllStudents();
        allStudents.forEach(
                consumerName.andThen(consumerActivities)

        );
    }

    public static void printStudentNameAndActivitiesUsingCondition() {
        java.util.List<Student> allStudents = StudentDatabase.getAllStudents();
        allStudents.forEach(student -> {
                    if (student.getGradeLevel() >= 3 && student.getGpa() >=3.9) {
                        consumerName.andThen(consumerActivities).accept(student);
                    }
                }
        );
    }
}