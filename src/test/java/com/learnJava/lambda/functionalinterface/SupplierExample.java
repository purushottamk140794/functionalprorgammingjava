package com.learnJava.lambda.functionalinterface;

import data.Student;
import data.StudentDatabase;

import java.util.Arrays;
import java.util.function.Supplier;

public class SupplierExample {

    static Supplier<Student> studentSupplier = () -> new Student("Adam", 2, 3.6, "male", 10, Arrays.asList("swimming", "basketball", "volleyball"));
    static Supplier<java.util.List<Student>> listOfStudents = () -> StudentDatabase.getAllStudents();

    public static void main(String[] args) {
        System.out.println(
                studentSupplier.get());


        System.out.println(

                listOfStudents.get()

        );
    }
}
