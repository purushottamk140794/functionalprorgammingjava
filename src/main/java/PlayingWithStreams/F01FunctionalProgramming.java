package PlayingWithStreams;

import java.util.Comparator;
import java.util.List;

public class F01FunctionalProgramming {
    public static void main(String args[]){
        List<Integer> number=List.of(1,3,5,4,2,1,7,4,3);
        List<String> courses=List.of("Azure","Java","Spring Boot","Python","Spring");

        //System.out.println(sumOfSquareOfNumber(number));
        //System.out.println(sumOfCubeOfOddNumber(number));
        //uniqueNumber(number);
        //sortNumber(number);
        //sortNumberWithReverseOrder(courses);
        sortNumberWithLengthOrder(courses);
    }
    public static int sumOfSquareOfNumber(List<Integer> numbers){
        return numbers.stream().map(x->x*x).reduce(0, Integer::sum);
    }
    public static int sumOfCubeOfOddNumber(List<Integer> numbers){
        return numbers.stream().filter(x->x%2!=0).map(x->x*x).reduce(0, Integer::sum);
    }
    public static void uniqueNumber(List<Integer> numbers){
         numbers.stream().distinct().forEach(System.out::println);
    }
    public static void sortNumber(List<Integer> numbers){
        numbers.stream().distinct().sorted().forEach(System.out::println);
    }
    public static void sortNumberWithReverseOrder(List<String> courses){
        courses.stream().sorted(Comparator.reverseOrder()).forEach(System.out::println);
    }
    public static void sortNumberWithLengthOrder(List<String> courses){
        courses.stream().sorted(Comparator.comparing(String::length)).forEach(System.out::println);
    }
}
