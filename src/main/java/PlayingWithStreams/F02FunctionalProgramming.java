package PlayingWithStreams;

import java.util.List;
import java.util.stream.Collectors;

public class F02FunctionalProgramming {
    public static void main(String[] args) {
        List<Integer> number = List.of(5, 5, 2, 7, 9, 2, 3);
        List<Integer> doubleNumber = doubleList(number);
        List<Integer> doubleEvenNumberList = doubleEvenNumberList(number);

        //System.out.println(doubleNumber);
        System.out.println(doubleEvenNumberList);

    }

    private static List<Integer> doubleList(List<Integer> number) {
        return number.stream().map(x -> x * x).collect(Collectors.toList());
    }

    private static List<Integer> doubleEvenNumberList(List<Integer> number) {
        return number.stream().filter(x -> x % 2 == 0).map(x -> x * x).collect(Collectors.toList());
    }
}
