package Vinoth_Udemy_FP;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FlatMap {
    public static void main(String[] args) {
        List<Integer> a=new ArrayList<>();
        a.add(1);
        a.add(2);
        a.add(3);
        List<Integer> b=new ArrayList<>();
        b.add(4);
        b.add(5);
        b.add(6);
        List<Integer> c=new ArrayList<>();
        c.add(7);
        c.add(8);
        c.add(9);
        List<List<Integer>> d=new ArrayList<>();
        d.add(a);
        d.add(b);
        d.add(c);

        d.stream().flatMap(Collection::stream)
                .forEach(System.out::println);
    }
}
