package Vinoth_Udemy_FP;

public class Assignment02 {
    public static void main(String[] args) {

        int onscreenNumber=0;

        int consumer=calculate(onscreenNumber,Integer::sum,5);
        consumer=calculate(consumer,Integer::sum,2);
        consumer=calculate(consumer,(a,b)->a-b,3);
        consumer=calculate(consumer,(a,b)->a*b,7);
        consumer=calculate(consumer,Integer::sum,2);
        consumer=calculate(consumer,(a,b)->a/b,3);

        System.out.println("Result is ::"+consumer);

    }
    public static int calculate(int onScreenNumber,Calculate calculate,int enteredNumber){
        return calculate.operate(onScreenNumber,enteredNumber);
    }
}
