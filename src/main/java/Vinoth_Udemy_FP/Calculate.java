package Vinoth_Udemy_FP;
@FunctionalInterface
public interface Calculate {
   int operate(int a, int b);
}
