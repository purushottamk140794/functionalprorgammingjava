package Vinoth_Udemy_FP;

import java.util.*;
public class Assignment03 {
    private static final Map<String,Calculate> map=new HashMap<>();
   static  {
        map.put("+", Integer::sum);
        map.put("-", (a, b) -> a - b);
        map.put("*", (a, b) -> a * b);
        map.put("/", (a, b) -> a / b);
    }
    public static void main(String[] args) {

        String number = "5 + 2 - 3 * 7 + 2 - 10";
        String[] eachDit = number.split(" ");
        int onScreenVal=Integer.parseInt(eachDit[0]);

        for (int i = 1; i < eachDit.length; i=i+2) {
            int enteredVal=Integer.parseInt(eachDit[i+1]);
            String op=eachDit[i];
            onScreenVal=calculate(onScreenVal,map.get(op),enteredVal);

        }
            System.out.println("Result is ::" + onScreenVal);
        }

        public static int calculate ( int onScreenNumber, Calculate calculate,int enteredNumber){
            return calculate.operate(onScreenNumber, enteredNumber);
        }
}
