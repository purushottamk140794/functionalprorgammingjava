package Vinoth_Udemy_FP;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class JavaFunction {
    public static void main(String[] args) {
        Supplier<Double> sup = Math::random;
        //System.out.println(sup.get());

        Consumer<String> con = (s) -> {
            System.out.println("value of S is " + s);

        };
        Consumer<String> con2 = (y) -> {
            System.out.println("value of Y is " + y);

        };
        con.andThen(con2).accept("22222");
    }
}
