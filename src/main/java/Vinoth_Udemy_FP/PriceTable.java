package Vinoth_Udemy_FP;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class PriceTable {

    private final WebDriver driver;

    @FindBy(css = "table#prods tbody tr")
    private List<WebElement> rows;

    @FindBy(css = "[type='button']")
    private WebElement verifyButton;

    @FindBy(id = "status")
    private WebElement txtStatus;

    public PriceTable(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void selectMinPrice() {
        Optional<List<WebElement>> minRowList = rows.stream()
                .skip(1)
                .map(tdList -> tdList.findElements(By.xpath("td")))
                .min(Comparator.comparing(td -> Integer.parseInt(td.get(2).getText())));
        if (minRowList.isPresent()) {
            List<WebElement> rows = minRowList.get();
            rows.get(3).findElement(By.tagName("input")).click();
            this.verifyButton.click();
        }
    }
    public String isStatus(){
        return this.txtStatus.getText();
    }


}
