package Vinoth_Udemy_FP;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AmazonPaymentScreen {
    private  WebDriver driver;

    @FindBy(id = "coupon")
    private WebElement couponCode;

    @FindBy(css = "[placeholder='VISA']")
    private WebElement inputAccountNumber;

    @FindBy(id = "year")
    private WebElement inputYear;

    @FindBy(id = "cvv")
    private WebElement inputCvv;

    @FindBy(xpath = "//*[@value='Buy Now']")
    private WebElement btnBuy;

    @FindBy(xpath = "//*[@id='status']")
    private WebElement status;

    @FindBy(xpath = "//*[@id='price']")
    private WebElement totalPrice;

    @FindBy(xpath = "//*[@value='apply']")
    private WebElement btnApply;

    public AmazonPaymentScreen(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void addCoupon(String val){
        couponCode.sendKeys(val);
        btnApply.click();
    }
    public void clickOnBuyButton(){
        btnBuy.click();
    }

    public void enterAccountDetail(String cc,String yr,String cvv){
        inputAccountNumber.sendKeys(cc);
        inputYear.sendKeys(yr);
        inputCvv.sendKeys(cvv);
    }
    public String verifyStatus(){
        return status.getText().trim();
    }
}
