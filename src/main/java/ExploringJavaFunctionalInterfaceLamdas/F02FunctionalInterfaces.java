package ExploringJavaFunctionalInterfaceLamdas;

import java.util.List;
import java.util.function.BinaryOperator;

public class F02FunctionalInterfaces {
    public static void main(String[] arg) {
        List<Integer> numbers = List.of(4, 3, 6, 9, 1, 4);
        BinaryOperator<Integer> sum = new BinaryOperator<Integer>() {
            @Override
            public Integer apply(Integer a, Integer b) {
                return a+b;
            }
        };
        int sumOfList=numbers.stream().reduce(0, sum);
        System.out.println(sumOfList);
    }
}
