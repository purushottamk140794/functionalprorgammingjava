package ExploringJavaFunctionalInterfaceLamdas;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class F01FunctionalInterfaces {
    public static void main(String[] arg) {
        List<Integer> numbers = List.of(4, 3, 6, 9, 1, 4);

        Predicate<Integer> integerPredicate = x -> x % 2 == 0;
        Predicate<Integer> integerPredicate2 = new Predicate<Integer>() {
            @Override
            public boolean test(Integer x) {
                return x % 2 == 0;
            }
        };

        Function<Integer, Integer> integerFunction = x -> x * x;
        Function<Integer, Integer> integerFunction2 = new Function<Integer, Integer>() {
            @Override
            public Integer apply(Integer x) {
                return x * x;
            }
        };

        Consumer<Integer> integerConsumer = System.out::println;
        Consumer<Integer> integerConsumer2 = new Consumer<Integer>() {
            @Override
            public void accept(Integer x) {
                System.out.println(x);
            }
        };


        numbers.stream().filter(integerPredicate2).
                map(integerFunction2).forEach(integerConsumer2);

    }
}
