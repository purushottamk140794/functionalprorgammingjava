package ExploringJavaFunctionalInterfaceLamdas;

import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;

public class F03BehaviourParameterization {
    public static void main(String[] arg) {
        Predicate<Integer> evenPredicate=x -> x % 2 == 0;
        Predicate<Integer> oddPredicate=x -> x % 2!= 0;

        List<Integer> numbers = List.of(4, 3, 6, 9, 1, 4);


        extractedFilterAndPrint(evenPredicate, numbers);
        extractedFilterAndPrint(oddPredicate, numbers);

    }

    private static void extractedFilterAndPrint(Predicate<Integer> evenPredicate, List<Integer> numbers) {
        numbers.stream().filter(evenPredicate)
                .forEach(System.out::println);
    }


}
